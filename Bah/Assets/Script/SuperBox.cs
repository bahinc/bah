﻿using UnityEngine;
using System.Collections;

public class SuperBox : MonoBehaviour {

    private const int nbOfPowerUp = 3;

    private const float beginHeight = 6.25f;
    
    private float FallSpeed;

    private GameObject _go;

    private PowerUp _powerUp;

    private GameManager _gm;

    private bool gameIsOver = false;

	public void init(float boundaries, GameManager gm)
    {
        _go = gameObject;
        _gm = gm;
        _powerUp = createARandomPowerUp();
        FallSpeed = Random.Range(1.5f, 3f);
        float posX = Random.Range(-boundaries, boundaries);
        transform.position = new Vector3(posX, beginHeight, 0);
    }

    public void stopTheBox()
    {
        gameIsOver = true;
    }
	
	// Update is called once per frame
	void Update () {
        if(!gameIsOver) _go.transform.position += Vector3.down * FallSpeed * Time.deltaTime;
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        //Debug.Log("I Triggered with an object of layer : " + other.gameObject.layer.ToString());
        if (other.gameObject.layer >= 8 && other.gameObject.layer <= 9 && !gameIsOver)
        {
            player2D player = other.gameObject.GetComponent<player2D>();
            if (player != null && !player.isHeDead) _powerUp.DoEffect(player);
            _gm.removeSuperBox(this);
        }
        else if (other.gameObject.layer == 11) _gm.removeSuperBox(this);
    }

    private PowerUp createARandomPowerUp()
    {
        int random = Random.Range(1, nbOfPowerUp);
        PowerUp pwUp;
        switch (random)
        {
            //Invicibility
            case 1:
                pwUp = new Invincibility();
                break;

            case 2:
                pwUp = new TripleJump();
                break;

            case 3:
                pwUp = new Teleportation();
                break;

            default:
                pwUp = null;
                break;
        }
        return pwUp;
    }
}
