﻿using UnityEngine;
using System.Collections;

public abstract class PowerUp {

    public abstract void DoEffect(player2D player);
    
}
