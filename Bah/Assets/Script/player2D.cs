﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(Controller2D))]
public class player2D : MonoBehaviour {

    public string name;

    public float maxJumpHeight = 15;
    public float minJumpHeight = 1;
    public float timeToJumpApex = 1f;
    public float accelerationTimeAirborne = 12f;
    public float accelerationTimeGrounded = 8f;
    float moveSpeed = 6;

    public float accelerationTimeToBottom;
    public int playerNumber;
    [SerializeField]
    private string numController;
    private bool isUsingKeyboard;
    public float deadZone = 0.7f; // for joystick's controller

    [SerializeField]
    private Vector2 Normalcollidersize = new Vector2(0.68f, 0.99f); // normal collider (standing)

    [SerializeField]
    private Vector2 deadcollidersize = new Vector2(0.99f, 0.5f); // collider when dead (body lying != standing)

    float gravity;
    float maxJumpVelocity;
    float minJumpVelocity;
    public Vector3 velocity;
    float velocityXSmoothing;

    public bool isGrounded = false;
    public bool didHeDoubleJump = false;
    public bool didHeTripleJump = false;

    public bool isInvicible = false;
    public bool canTripleJump = false;
    private bool isDodgeInCd = false;
    private int tpAllowed = 0;

    public float dodgeDuration = 0.5f;
    public float dodgeCd = 0.75f;

    private bool isDead = false;
    public bool isHeDead
    {
        private set { }
        get { return isDead; }
    }
    public bool isStun = false;
    //public bool test = false;
    public float stunJumpVelocity = 12f; // rejected force on the Y-Axis
    public float stunForceVelocity = 2f; // rejected force on the X-Axis


    public bool allowToMove = false;

    Controller2D controller;
    Animator _animator;
    SpriteRenderer _spriteRenderer;
    BoxCollider2D _collider;

    public GameManager manager;

    public void InitPlayer(GameManager _m, int _nPl, string howPlay)
    {
        manager = _m;
        playerNumber = _nPl;
        numController = howPlay;
        isUsingKeyboard = numController.Contains("Keyboard");
    }

    void Start()
    {
        controller = GetComponent<Controller2D>();
        _animator = GetComponent<Animator>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _collider = GetComponent<BoxCollider2D>();

        gravity = -(2 * maxJumpHeight) / Mathf.Pow(timeToJumpApex, 2);
        maxJumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
        minJumpVelocity = Mathf.Sqrt(2 * Mathf.Abs(gravity) * minJumpHeight);
    }

    public void Reset()
    {
        controller.ResetCollisionMask();
        _collider.offset = Vector2.zero;
        _collider.size = Normalcollidersize; 
        isDead = false;
        isStun = false;
        tpAllowed = 0;
        foreach (AnimatorControllerParameter b in _animator.parameters) _animator.SetBool(b.name, false);
        _animator.SetBool("idle", true);
    }

    public void CanYouMove(bool allow)
    {
        allowToMove = allow;
    }

    public void ResetIdleBool()
    {
        _animator.SetBool("idle", false);
    }

    //Powers Up {

    public void setInvincibility(float duration)
    {
        Debug.Log(name + " is now invicible");
        StartCoroutine(IamInvicible(duration));
    }

    public void setTripleJump(float duration)
    {
        Debug.Log(name + " can now Triple Jump");
        StartCoroutine(IcanTripleJump(duration));
    }

    public void setTeleportation(int numberOfTp)
    {
        Debug.Log(name + " has now " + numberOfTp + " more tp");
        Debug.Log("ça marche pas encore cependant ! :D");
        tpAllowed += numberOfTp;
    }

    //}

    public void DeathAnim(float _dir)
    {
        controller.SetCollisionMaskToGround();
        StartCoroutine(Fall(_dir, ReasonToFall.death));
    }

    public void removePlayerFromCollision(string layerName)
    {
        controller.RemovePlayerFromCollisionMask(layerName);
    }

    public void GetStun(float _dir)
    {
        StartCoroutine(Fall(_dir, ReasonToFall.stun));
    }

    void Update()
    {

        if (!isStun && !isDead)
        {
            Vector2 input = new Vector2(isUsingKeyboard ? RebindableInput.GetAxis("Horizontal" + numController) : Input.GetAxisRaw("Horizontal" + numController), 0);
            input.x = Mathf.Abs(input.x) < deadZone ? 0 : input.x;

            velocity.x = input.x * ((controller.collisions.below) ? accelerationTimeGrounded : accelerationTimeAirborne);
            _animator.SetBool("run", velocity.x != 0 && isGrounded);
            if (_animator.GetBool("jump A")) _animator.SetBool("jump A", false);
            if (RebindableInput.GetKeyDown("Jump" + numController))
            {
                if (controller.collisions.below || !didHeDoubleJump || (canTripleJump && !didHeTripleJump))
                {
                    didHeTripleJump = didHeDoubleJump && !controller.collisions.below;
                    didHeDoubleJump = !controller.collisions.below;
                    if (_spriteRenderer.sprite.name == "jump_1")
                    {
                        //_animator.SetBool("jump C", false);
                        //_animator.Play("jump A");
                        _animator.CrossFade("jump A", 0f, 0);
                    }
                    else _animator.SetBool("jump A", true);
                    _animator.SetBool("run", false);
                    _animator.SetBool("jump C", false);
                    velocity.y = maxJumpVelocity;
                }
            }

            if (RebindableInput.GetKeyUp("Jump" + numController))
            {
                if (velocity.y > minJumpVelocity)
                {
                    _animator.SetBool("jump B", true);
                    velocity.y = minJumpVelocity;
                }
            }
            if (velocity.y < 0 && !_animator.GetBool("jump B")) _animator.SetBool("jump B", true);

            if (RebindableInput.GetKeyDown("Dodge" + numController) && !isDodgeInCd) StartCoroutine(Dodge());

            if (RebindableInput.GetKeyDown("Stomp" + numController) /*&& velocity.y < 0*/) velocity.y += -accelerationTimeToBottom;
        }

        if (allowToMove)
        {
            velocity.y += gravity * Time.deltaTime;
            //Debug.Log(playerNumber + " : velocity before Move = " + velocity);
            controller.Move(velocity * Time.deltaTime);

            isGrounded = controller.collisions.below;
            if (isGrounded && isStun) isStun = false; // When the player touches the ground after falling for stun
            if (isGrounded && isDead) // When the player touches the ground after falling for death
            {
                allowToMove = false;
                _animator.SetBool("death", false);
                manager.RemoveMeFromTheRound(this);
                velocity = Vector3.zero;
            }

            if (controller.collisions.above || isGrounded)
            {
                if (_animator.GetBool("jump C") && velocity.y > -0.3f) _animator.SetBool("jump C", false);
                if (isGrounded && velocity.y < -0.5f)
                {
                    //Debug.Log(velocity.y);
                    _animator.SetBool("jump C", true);
                }
                if (controller.hitInfo.gameObject.tag == "Player")
                {
                    player2D touchedPlayer = controller.hitInfo.gameObject.GetComponent<player2D>();
                    if (controller.collisions.below)
                    {
                        if (touchedPlayer.isInvicible)
                        {
                            manager.StunPlayer(touchedPlayer, this);
                        }
                        else if (!isStun)
                        {
                            Debug.Log(touchedPlayer + " has been Killed");
                            manager.KillPlayerByPlayer(touchedPlayer, this);
                        }
                    }
                    else
                    {
                        if (this.isInvicible)
                        {
                            manager.StunPlayer(this, touchedPlayer);
                        }

                        //Deux Cas de mort, might be dangerous.
                        /*else if (!touchedPlayer.isStun)
                        {
                            manager.KillPlayerByPlayer(this, touchedPlayer);
                        }*/
                    }
                }
                else velocity.y = 0;
            }
        }
    }

    private IEnumerator Dodge()
    {
        isDodgeInCd = true;
        isInvicible = true;
        yield return new WaitForSeconds(dodgeDuration);
        isInvicible = false;
        yield return new WaitForSeconds(dodgeCd);
        isDodgeInCd = false;
    }

    private IEnumerator Fall(float direction, ReasonToFall reason)
    {
        isStun = reason == ReasonToFall.stun;
        isDead = reason == ReasonToFall.death;
        _animator.SetBool("death", isDead);
        if(isDead)
        {
            _collider.offset = new Vector2(-0.21f, 0);
            _collider.size = deadcollidersize;
        }
        velocity.y += stunJumpVelocity;
        //velocity.x = velocity.x == 0 ? Mathf.Sign(Random.Range(-1, 1)) * stunForceVelocity : velocity.x;
        velocity.x = Mathf.Sign(direction) * stunForceVelocity;
        yield return null;
    }

    public enum ReasonToFall
    {
        stun = 1,
        death = 2
    }

    //Coroutine Powers Up {

    private IEnumerator IamInvicible(float duration)
    {
        isInvicible = true;
        yield return new WaitForSeconds(duration);
        isInvicible = false;
    }

    private IEnumerator IcanTripleJump(float duration)
    {
        canTripleJump = true;
        yield return new WaitForSeconds(duration);
        canTripleJump = false;
    }

    //}
}
