﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GuiGame : MonoBehaviour {

    private List<player2D> _listPlayers;

    private List<GameObject> _listeCoeurs;

    private Dictionary<player2D, List<GameObject>> _listOfheartLists;

    private GameManager _manager;

    // Use this for initialization
    public void Init (List<player2D> listesPlayers, GameManager gameManager)
    {
        _listPlayers = listesPlayers;
        _manager = gameManager;
        _listOfheartLists = new Dictionary<player2D, List<GameObject>>();
        for(int i = 0; i < _listPlayers.Count; i++)
        {
            GameObject container = Instantiate(Resources.Load("containerCoeur") as GameObject);
            container.name = _listPlayers[i].name;
            container.GetComponentInChildren<Text>().text = _listPlayers[i].name;
            List<GameObject> listesCoeurs = new List<GameObject>();
            for (int y = 0; y < 5; y++)
            {
                listesCoeurs.Add(Instantiate(Resources.Load("heart") as GameObject));
                listesCoeurs[y].transform.SetParent(container.transform.FindChild("coeurs"));
            }
            _listOfheartLists.Add(_listPlayers[i], listesCoeurs);
            container.transform.SetParent(transform);
        }
    }

    public void removeHeart(player2D _player)
    {
        List<GameObject> _listTemp = _listOfheartLists[_player];
        _listTemp[_listTemp.Count - 1].GetComponent<RawImage>().enabled = false;
        _listTemp.Remove(_listTemp[_listTemp.Count - 1]);
        _listOfheartLists[_player] = _listTemp;
    }


    /*public void hit(GameObject _player)
    {
        if (_player.GetComponent<player2D>().playerNumber == 1) coeur1[actualcoeur1++].SetActive(false);

        if (_player.gameObject.GetComponent<player2D>().playerNumber == 2) coeur2[actualcoeur2++].SetActive(false);
    }*/

   
}
