﻿using UnityEngine;
using System.Collections;
using System;

public class Teleportation : PowerUp {

    private int nbOfTp = 3;

    public override void DoEffect(player2D player)
    {
        player.setTeleportation(nbOfTp);
    }
}
