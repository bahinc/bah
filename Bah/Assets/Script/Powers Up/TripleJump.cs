﻿using UnityEngine;
using System.Collections;
using System;

public class TripleJump : PowerUp {

    private float duration = 5f;

    public override void DoEffect(player2D player)
    {
        player.setTripleJump(duration);
    }
}
