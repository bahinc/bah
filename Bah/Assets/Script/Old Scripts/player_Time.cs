﻿using UnityEngine;
using System.Collections;

public class player_Time : MonoBehaviour {
    
    public float speed = 6.0f;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;

    [SerializeField]
    private CharacterController controller;
    //public float rotateSpeedGrounded = 3.0f; // Rotation speed at ground

    public float inAirSpeed = 2.0f; // Adds or substracts to the movementspeed in air
    public float inAirDrift = 0.5f; // Adds Sidewards drift in air

    public float deadZone = 0.15f;
    public float extraJumpForce = 5.0f;         //How much extra force to give to our jump when the button is held down
    public float maxExtraJumpTime = 2.0f;       //Maximum amount of time the jump button can be held down for
    public float delayToExtraJumpForce = 2.0f;  //Delay in how long before the extra force is added
    private float jumpTimer;             //Used in calculating the extra jump delay
    private float timeHolding;
    private bool playerJumped = false;           //Tell us if the player has jumped
    private bool playerJumping = false;          //Tell us if the player is holding down the jump button

    private Vector2 moveDirection = Vector2.zero;
    private float inputModifyFactor = 1.0f;
    public bool isGrounded = false;
    public Transform groundChecker;
    private bool pressedJump = false;
    private bool unpressedJump = false;

    public bool doubleJumped = false;
    private bool didHeDoubleJumped = false;

    void Update()
    {
        pressedJump = Input.GetButtonDown("Jump");
        unpressedJump = Input.GetButtonUp("Jump");
    }

    //void OnControllerColliderHit(ControllerColliderHit hit)
    //{
    //    if (hit.collider.name == "sol" && !pressedJump)
    //    {
    //        Debug.Log("touched the ground");
    //        //isGrounded = true;
    //    }
    //}

    void FixedUpdate()
    {

        //isGrounded = Physics.Linecast(transform.position, groundChecker.position);
        //Debug.Log(controller.collisionFlags + " " + controller.);
        //if ((controller.collisionFlags & CollisionFlags.Below) != 0)
        //    Debug.Log(isGrounded + " | " + controller.isGrounded);
        float rayDistance = controller.bounds.extents.y;
        isGrounded = Physics.Raycast (transform.position, Vector3.down, rayDistance + 0.085f);

        //If our player hit the jump key, then it's true that we jumped!
        //Debug.Log(Vector2.Distance(transform.position, groundChecker.position));
        if (pressedJump)
        {
            if (isGrounded)
            {
                didHeDoubleJumped = false;
                isGrounded = false;
                pressedJump = false;
                playerJumped = true;   //Our player jumped!
                playerJumping = true;  //Our player is jumping!
                jumpTimer = Time.time; 
                timeHolding = Time.time; //Set the time at which we jumped
            }
            else
            {
                if (!didHeDoubleJumped)
                {
                    didHeDoubleJumped = true;
                    doubleJumped = true;    //Our player doublejumped!
                    playerJumping = true;  //Our player is jumping!
                    pressedJump = false;
                    jumpTimer = Time.time;
                }
            }
        }

        //If our player lets go of the Jump button OR if our jump was held down to the maximum amount...
        if (unpressedJump || Time.time - jumpTimer > maxExtraJumpTime)
        {
            unpressedJump = false;
            playerJumping = false; //... then set PlayerJumping to false
        }

        //            // ---------------------------------------- At Ground -----------------------------------------
        if (isGrounded)
        {
            moveDirection = new Vector2(Input.GetAxis("Horizontal"), 0);
            moveDirection.x = Mathf.Abs(moveDirection.x) < deadZone ? 0 : moveDirection.x;
            moveDirection *= speed;
            //inputModifyFactor = (moveDirection.x != 0.0f && moveDirection.z != 0.0f) ? 0.7071f : 1.0f;
            //moveDirection *= inputModifyFactor;

            if (playerJumped || doubleJumped)
            {
                moveDirection.y = jumpSpeed;
                playerJumped = false;
                doubleJumped = false;
            }
        }
        // ---------------------------------------------------- In Air  ----------------------------
        else
        {
            // We are not grounded. We can still influence the movement with the horizontal and vertical axis, but not so strong.
            moveDirection = new Vector2(Input.GetAxis("Horizontal"), moveDirection.y);
            moveDirection.x *= inAirSpeed;

            if (doubleJumped)
            {
                moveDirection.y = jumpSpeed;
                doubleJumped = false;
            }
        }
        // ---------------------------------------------------------------------------------------------        
        ////Rotate
        //Vector3 curDir = new Vector3(moveDirection.x, 0, moveDirection.z);
        //Vector3 newDir = Vector3.RotateTowards(transform.forward, curDir, (isGrounded ? rotateSpeedGrounded : inAirRotation) * Time.deltaTime, 0.0F);
        //Debug.DrawRay(transform.position, newDir * 10, Color.red);
        //transform.rotation = Quaternion.LookRotation(newDir);

        //Move the controller
        if (moveDirection != Vector2.zero)
        {
            if (playerJumping && Time.time - jumpTimer > delayToExtraJumpForce)
            {
                moveDirection.y = moveDirection.y + extraJumpForce;
                //jumpTimer = Time.time;
                playerJumping = false;
            }
            controller.Move(moveDirection * Time.deltaTime);
        }
    }
}
