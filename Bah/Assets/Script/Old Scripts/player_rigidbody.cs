﻿using UnityEngine;
using System.Collections;

public class player_rigidbody : MonoBehaviour
{
    [SerializeField]
    private Rigidbody _rigid;

    [SerializeField]
    private bool isGrounded;

    public float speed = 6.0f;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;
    public float deadZone = 0.15f;

    private Vector2 moveDirection = Vector2.zero;
    private bool playerJumped = false;           //Tell us if the player has jumped
    private bool playerJumping = false;
    public float extraJumpForce = 5.0f;         //How much extra force to give to our jump when the button is held down
    public float maxExtraJumpTime = 2.0f;       //Maximum amount of time the jump button can be held down for
    public float delayToExtraJumpForce = 2.0f;  //Delay in how long before the extra force is added

    private bool pressedJump = false;
    private bool unpressedJump = false;

    private float jumpTimer;             //Used in calculating the extra jump delay

    private float rayDistance = 0.5f/*controller.bounds.extents.y*/;

    // in player.init

    void Start()
    {
        //this.jumpTimer = new ig.Timer(this.jumpBoostTime * 0.001);
        //this.jumpTimer.pause();
    }

    // ....

    // in player.update
    void Update()
    {
        pressedJump = Input.GetButtonDown("Jump");
        unpressedJump = Input.GetButtonUp("Jump");



        //if (this.standing && ig.input.pressed('jump'))
        //{
        //    this.vel.y = -this.jump;
        //}

        //if (!this.standing && ig.input.state('jump') && this.vel.y < 0 && this.jumpTimer.delta() < 0)
        //{
        //    this.accel.y = this.jumpBoost * (this.jumpTimer.delta() / (this.jumpBoostTime * 0.001));
        //}
        //else
        //{
        //    this.accel.y = 0;
        //}
    }

    void FixedUpdate()
    {
        isGrounded = Physics.Raycast(transform.position, Vector3.down, rayDistance + 0.085f);

        if (pressedJump)
        {
            if (isGrounded)
            {
                isGrounded = false;
                pressedJump = false;
                playerJumped = true;   //Our player jumped!
                playerJumping = true;  //Our player is jumping!
                jumpTimer = Time.time;
                //timeHolding = Time.time; //Set the time at which we jumped
            }
        }

        //If our player lets go of the Jump button OR if our jump was held down to the maximum amount...
        if (unpressedJump || Time.time - jumpTimer > maxExtraJumpTime)
        {
            unpressedJump = false;
            playerJumping = false; //... then set PlayerJumping to false
        }

        //            // ---------------------------------------- At Ground -----------------------------------------
        if (isGrounded)
        {
            moveDirection = new Vector2(Input.GetAxis("Horizontal"), 0);
            moveDirection.x = Mathf.Abs(moveDirection.x) < deadZone ? 0 : moveDirection.x;
            moveDirection *= speed;
            //inputModifyFactor = (moveDirection.x != 0.0f && moveDirection.z != 0.0f) ? 0.7071f : 1.0f;
            //moveDirection *= inputModifyFactor;

            if (playerJumped)
            {
                moveDirection.y = jumpSpeed;
                playerJumped = false;
            }
        }
        // ---------------------------------------------------- In Air  ----------------------------
        else
        {
            // We are not grounded. We can still influence the movement with the horizontal and vertical axis, but not so strong.
            moveDirection = new Vector2(Input.GetAxis("Horizontal"), moveDirection.y);
            moveDirection.x *= speed;
            moveDirection.y -= gravity * Time.deltaTime;
        }
        // ---------------------------------------------------------------------------------------------   

        if (moveDirection != Vector2.zero)
        {
            if (playerJumping && Time.time - jumpTimer > delayToExtraJumpForce)
            {
                moveDirection.y = moveDirection.y + extraJumpForce;
                jumpTimer = Time.time;
                //playerJumping = false;
            }
            _rigid.velocity = moveDirection;
        }
    }
}
