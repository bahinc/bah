﻿using UnityEngine;
using System.Collections;

public class player : MonoBehaviour {

    [SerializeField]
    private GameManager _gameManager;

    public float maxJumpHeight = 35;
    public float minJumpHeight = 1;
    public float timeToReachMaxHeight = .8f;
    public float moveSpeed = 12;

    private int verticalRayCount = 4;
    private float verticalRaySpacing;

    public float deadZone = 0.7f; // for the joystick

    public float skinHeight = 4;

    public int playerNumber;

    private Animator _animator;

    [SerializeField]
    private Controller2D controller;

    [SerializeField]
    private BoxCollider2D _feetCol;

    [SerializeField]
    private BoxCollider2D _headCol;

    private float _gravity;
    private float _maxjumpVelocity;
    private float _minJumpVelocity;
    private Vector2 moveDirection;
    [SerializeField]
    private bool playerJumped = false;   //Tell us if the player has jumped

    private bool pressedJump = false;
    private bool unpressedJump = false;
    private bool pressedLeftDash = false;
    private bool pressedRightDash = false;

    public float inAirSpeed = 8.0f; // Adds or substracts to the movementspeed in air

    private bool doubleJumped = false;
    public bool didHeDoubleJumped = false;

    public bool isGrounded = false;

    private float rayDistance;

    void Start()
    {
        controller = GetComponent<Controller2D>();
        _gravity = -(2 * maxJumpHeight) / Mathf.Pow(timeToReachMaxHeight, 2);
        _maxjumpVelocity = Mathf.Abs(_gravity) * timeToReachMaxHeight;
        _minJumpVelocity = Mathf.Sqrt(Mathf.Abs(_gravity) * minJumpHeight);
        //rayDistance = controller.bounds.extents.y;
        rayDistance = 0.485f;
        //verticalRaySpacing = (controller.bounds.max.x - controller.bounds.min.x) / verticalRayCount;
        _animator = GetComponent<Animator>();
        print("Gravity: " + _gravity + "  Jump Velocity: " + _maxjumpVelocity);
    }

    void Update()
    {
        if (Input.GetButtonDown("Jump" + playerNumber.ToString()) && !didHeDoubleJumped)
        {
            pressedJump = true;
        }

        if (Input.GetButtonDown("DashLeft" + playerNumber.ToString()))
        {
            pressedLeftDash = true;
        }
        if (Input.GetButtonDown("DashRight" + playerNumber.ToString()))
        {
            pressedRightDash = true;
        }
        unpressedJump = Input.GetButtonUp("Jump" + playerNumber.ToString());


        /*for (int i = 0; i < verticalRayCount; i++)
        {
            Vector2 rayOrigin = new Vector2(controller.bounds.min.x, controller.bounds.max.y);
            rayOrigin += Vector2.right * (verticalRaySpacing * i + moveDirection.x);
            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.down, rayDistance);

            Debug.DrawRay(rayOrigin, Vector2.down * rayDistance, Color.red);

            if (hit)
            {
                isGrounded = true;
                didHeDoubleJumped = false;
                break;
            }
        }*/
        isGrounded = Physics.Raycast(transform.position, Vector3.down, rayDistance + 0.085f);
        if (isGrounded) didHeDoubleJumped = false;
    }
    
    void FixedUpdate()
    {
        //
        if (pressedJump)
        {
            if (isGrounded)
            {
                pressedJump = false;
                playerJumped = true;   //Our player jumped!

                _animator.SetTrigger("jump");
                //playerJumping = true;  //Our player is jumping!
                //jumpTimer = Time.time;
                //timeHolding = Time.time; //Set the time at which we jumped
            }
            else
            {
                if (!didHeDoubleJumped)
                {
                    didHeDoubleJumped = true;
                    doubleJumped = true;    //Our player doublejumped!
                    _animator.SetTrigger("jump");
                    //playerJumping = true;  //Our player is jumping!
                    pressedJump = false;
                    //jumpTimer = Time.time;
                }
            }
        }
        //If our player lets go of the Jump button OR if our jump was held down to the maximum amount...
        if (unpressedJump)
        {
            unpressedJump = false;
            if(moveDirection.y > _minJumpVelocity) moveDirection.y = _minJumpVelocity;
            //playerJumping = false; //... then set PlayerJumping to false
        }

        //            // ---------------------------------------- At Ground -----------------------------------------
        if (isGrounded)
        {
            moveDirection = new Vector2(Input.GetAxis("Horizontal" + playerNumber.ToString()), 0);
            moveDirection.x = Mathf.Abs(moveDirection.x) < deadZone ? 0 : moveDirection.x;
            if (moveDirection.x != 0)
            {
                _animator.SetTrigger("run");
                transform.localEulerAngles = new Vector3(0, Mathf.Sign(moveDirection.x) < 0 ? 180 : 0, 0);
            }
            else _animator.SetTrigger("idle");
            moveDirection *= moveSpeed;

            if (playerJumped || doubleJumped)
            {
                playerJumped = false;
                doubleJumped = false;
                isGrounded = false;
                moveDirection.y = maxJumpHeight;
            }
        }
        // ---------------------------------------------------- In Air  ----------------------------
        else
        {
            // We are not grounded. We can still influence the movement with the horizontal and vertical axis, but not so strong.
            moveDirection = new Vector2(Input.GetAxis("Horizontal" + playerNumber.ToString()), moveDirection.y);
            moveDirection.x = Mathf.Abs(moveDirection.x) < deadZone ? 0 : moveDirection.x;
            if (moveDirection.x != 0) transform.localEulerAngles = new Vector3(0, Mathf.Sign(moveDirection.x) < 0 ? 180 : 0, 0);
            moveDirection.x *= inAirSpeed;

            if (doubleJumped)
            {
                moveDirection.y = maxJumpHeight;
                doubleJumped = false;
            }else
            {
                moveDirection.y += _gravity * Time.deltaTime; 
            }
        }
        // ---------------------------------------------------------------------------------------------  

        if (moveDirection != Vector2.zero)
        {
            //VerticalCollisions(ref moveDirection);
            //Debug.Log("moveDirection " + moveDirection);
            //Vector3 moveVector = new Vector3(moveDirection.x < 0?-moveDirection.x:moveDirection.x, moveDirection.y,0);
            //transform.Translate(moveVector * Time.deltaTime);
            controller.Move(moveDirection * Time.deltaTime);
        }
    }

    void OnCollision2DEnter(Collision2D col)
    {
        //Debug.Log("trigger " + col.gameObject.name + " | " + col.contactOffset /*col.contacts[0].otherCollider*/);
    }
    
    /*void OnControllerColliderHit(ControllerColliderHit hit)
    {
        //Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("player"), LayerMask.NameToLayer("bodypart"));
        //Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("player"), LayerMask.NameToLayer("player"));
        //if (hit.collider.name != "sol") Debug.Log(hit.controller.collisionFlags);
        //Debug.Log(Physics2D.GetIgnoreLayerCollision(LayerMask.NameToLayer("player"), LayerMask.NameToLayer("bodypart")).ToString());
        //if (moveDirection.y > 0 && hit.controller.collisionFlags != CollisionFlags.CollidedBelow && hit.controller.collisionFlags != CollisionFlags.CollidedSides)
        //Debug.Log(hit.controller.collisionFlags + " | " + hit.collider + " | " + hit.collider.tag);
        if((hit.controller.collisionFlags & CollisionFlags.CollidedAbove) != 0)
        {
            Debug.Log("you hit something above");
            moveDirection.y = 0;
            switch (hit.collider.tag)
            {
                case "Player":
                    _gameManager.KillPlayer(gameObject);
                    break;
            }
        }
        else if((hit.controller.collisionFlags & CollisionFlags.CollidedBelow) != 0 || (hit.controller.collisionFlags & CollisionFlags.None) != 0)
        {
            switch (hit.collider.tag)
            {
                case "Player":
                    _gameManager.KillPlayer(hit.collider.gameObject);
                    break;

                case "obstacle":
                    if (!playerJumped)
                    {
                        //Debug.Log("reset");
                        didHeDoubleJumped = false;
                        isGrounded = true;
                    }
                    break;

                default :
                    Debug.Log(hit.collider.tag);
                    break;
            }
        }
    }*/
}
