﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Model_MenuManager : MonoBehaviour {

    public GameObject model;

    private GameObject clone;

    public RawImage imageRond;

    //TODO - make Model thing alive
    public void startAnim()
    {
        clone = Instantiate(model);
        clone.GetComponent<SpriteRenderer>().flipX = transform.parent.name == "Right";
        clone.transform.SetParent(transform);
        clone.transform.localPosition = Vector3.back;
        clone.transform.localScale = Vector3.one * 100;
    }

    public void destroyModel()
    {
        Destroy(clone);
    }
}
