﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;

public class MenuManager : MonoBehaviour {
    public object SceneManagement { get; private set; }

    public Dictionary<int, Character> _listJoueursWCharacter;

    private int _numbPlayers = 0;

    private List<User> _listUser;

    private Dictionary<User, wayOfPlaying> _howDoUserPlay;

    private Dictionary<int, string> _howDoPlayerPlay;

    public Dictionary<int, Character> lesCharacters;
    

    private Interface_MenuManager interface_menu;

    [SerializeField] 
    private Canvas _myOwnCanvas;

    /*private bool playerOneCreated = false;

    private bool playerTwoCreated = false;
    */
    private void StartGame(Dictionary<int, Character> _lesPlayers)
    {
        SceneManager.LoadScene("GameLevel", LoadSceneMode.Additive);
        _myOwnCanvas.gameObject.SetActive(false);
        //Dictionary<int, string> _listPlayer = new Dictionary<int, string>();
        //foreach (Character _character in _lesPlayers) _listPlayer.Add(_character.name);
        GameManager _gameManager = gameObject.AddComponent<GameManager>();
        foreach(User u in _listUser) _howDoPlayerPlay.Add(u._numPlayer, _howDoUserPlay[u].ToString());
        _gameManager.Init(_listJoueursWCharacter, _howDoPlayerPlay);
        Destroy(this);
    }

    void Start()
    {
        Debug.Log("Salut Hugo, te voilà de retour dans BAH ! Beaucoup de choses ont changé depuis la dernière fois, mais il reste encore beaucoup à faire ! Tu pourras sans doute remarquer quelques bugs, n'hésites pas à me les dire ! Dans les nouveautés que tu pourras découvrir : Des Box : avec des supers pouvoirs qui sont pour l'instant au nombre incroyable de 3 ! (Bon en vrai 2, car il y en a un que j'ai pas implémenter) Et d'autres sont à venir, j'ai des idées qui ne demandent qu'à être intégrée, et si toi aussi tu en as, de même n'hésite pas à me les partagers ! Le Menu a aussi beaucoup changé comme tu peux le voir, et il est fonctionnel... en théorie, il ne me manque que des models et ça sera bon : C'est donc pour ça qu'il faut éviter de choisir autre chose que les deux premiers personnages ! Tu vas peut-être aussi remarquer que j'ai ajouter la possibilité d'avoir 4 joueurs, ce n'est actuellement pas implémenté dans le jeu, mais il me faut juste rajouter les inputs ! J'ai ainsi pensé à plusieurs mode de jeu dont je te ferais part plus tard ! Je vais bientôt faire un grand ménage dans le code car certains trucs sont vraiment mal écrit selon moi ! Voilà, si tu as des questions n'hésites pas !");
        _listUser = new List<User>();
        _howDoUserPlay = new Dictionary<User, wayOfPlaying>();
        _howDoPlayerPlay = new Dictionary<int, string>();
        _listJoueursWCharacter = new Dictionary<int, Character>();
        lesCharacters = new Dictionary<int, Character>();
        foreach (Character _c in _myOwnCanvas.GetComponentsInChildren<Character>())
        {
            lesCharacters.Add(_c.index, _c);
        }
        interface_menu = _myOwnCanvas.GetComponent<Interface_MenuManager>();
        interface_menu.Init(lesCharacters);
    }

    public void placeModel(User u, Character c)
    {
        interface_menu.switchModel(u, c);
    }

    public void changeTeam(User u, Team t)
    {
        interface_menu.switchTeamColor(u, t);
    }

    public void RequestOfStarting()
    {
        if (_listJoueursWCharacter.Count == _numbPlayers) StartGame(_listJoueursWCharacter);
    }

    public void SelectCharacter(int player,Character _character)
    { 
        if (player <= _numbPlayers)
        {
            if (!_character.selected) //on regarde si ce character a déjà été sélectionné
            {
                _listJoueursWCharacter.Add(player, _character);
                _character.selected = true;
                interface_menu.readyOrUnreadyUser(player, true);
                foreach (User _u in _listUser)
                    if (_u._numPlayer == player) _u.animSelect();
            }
            else {
                foreach (User _u in _listUser)
                    if (_u._numPlayer == player) _u.animCantSelect();
            }
        }
        else
        {
            Debug.LogError("index Of player above the number of players");
        }
    }

    public void UnselectCharacter(int player)
    {
        if (_listJoueursWCharacter.ContainsKey(player))
        {
            _listJoueursWCharacter[player].selected = false;
            foreach (User _u in _listUser)
                if (_u._numPlayer == player) _u.animDeselect();
            interface_menu.readyOrUnreadyUser(player, false);
            _listJoueursWCharacter.Remove(player);
        }
    }

    private void CreateUser(int index, wayOfPlaying way)
    {
        foreach (User _u in _listUser) if (_u._numPlayer == index) return;

        GameObject userGo = Instantiate(Resources.Load("Users/" + index) as GameObject);
        User user = userGo.AddComponent<User>();
        user.Init(this, index, lesCharacters[index], way.ToString());
        _listUser.Add(user);
        interface_menu.creationOrDestructionOfUser(user, true);
        _howDoUserPlay.Add(user, way);
        _numbPlayers++;
    }

    public void DestroyUser(User _user)
    {
        _howDoUserPlay.Remove(_user);
        _listUser.Remove(_user);
        interface_menu.creationOrDestructionOfUser(_user, false);
        Destroy(_user.gameObject);
        _numbPlayers--;
    }

    private void requestCreateUser(wayOfPlaying howDoYouWannaPlay)
    {
        if (_numbPlayers >= 2) return;
        foreach (User user in _listUser) if (_howDoUserPlay[user] == howDoYouWannaPlay) return;

        CreateUser(_numbPlayers + 1, howDoYouWannaPlay);
    }

    void Update()
    {
        if (RebindableInput.GetKeyDown("JumpController1")) requestCreateUser(wayOfPlaying.Controller1);
        if (RebindableInput.GetKeyDown("JumpController2")) requestCreateUser(wayOfPlaying.Controller2);
        if (RebindableInput.GetKeyDown("JumpKeyboard1")) requestCreateUser(wayOfPlaying.Keyboard1);
        if (RebindableInput.GetKeyDown("JumpKeyboard2")) requestCreateUser(wayOfPlaying.Keyboard2);
        /*foreach (KeyCode kcode in Enum.GetValues(typeof(KeyCode)))
        {
            if (Input.GetKeyDown(kcode))
                Debug.Log("KeyCode down: " + kcode);
        }*/
    }

    private enum wayOfPlaying
    {
        Controller1, Controller2, Keyboard1, Keyboard2
    }

    public enum Team
    {
        white, blue, red, yellow, green
    }
}
