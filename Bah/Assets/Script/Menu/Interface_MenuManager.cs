﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Interface_MenuManager : MonoBehaviour {

    [SerializeField]
    private ModelManager ModelManager;

    [SerializeField]
    private GameObject characterPanel;

    [SerializeField]
    private PlayerPanel panelPlayer;

    [SerializeField]
    private Dictionary<User, Model_MenuManager> lesModels;

    [SerializeField]
    private Dictionary<Character, GameObject> goFromResources; 

    public void Init(Dictionary<int, Character> lesCharacters)
    {
        int spaceBetweenCharacter = (int)(characterPanel.GetComponent<RectTransform>().rect.width - 500) / 4;
        characterPanel.GetComponent<GridLayoutGroup>().spacing = Vector2.one * spaceBetweenCharacter;

        ModelManager.init();

        lesModels = new Dictionary<User, Model_MenuManager>();
        goFromResources = new Dictionary<Character, GameObject>();
        foreach(Character c in lesCharacters.Values)
        {
            if (c.nom == "Johnny" || c.nom == "Philippe")
            {
                GameObject go = Resources.Load("players/" + c.nom) as GameObject;
                go.GetComponent<player2D>().enabled = false;
                if (go != null)
                {
                    goFromResources.Add(c, go);
                    Debug.Log(c.nom + " est maintenant dans le dictionnaire");
                }
            }
        }
    }

    public void creationOrDestructionOfUser(User user, bool isItCreation)
    {
        panelPlayer.activeOrDesactiveUser(user, isItCreation);

        ModelManager.activeOrDesactiveUser(user, isItCreation);

        if (isItCreation)
        {
            Model_MenuManager model = ModelManager.getModel(user);
            lesModels.Add(user, model);
            switchModel(user, user.choosenCharacter);
        }
    }

    public void switchModel(User u, Character c)
    {
        lesModels[u].destroyModel();
        lesModels[u].model = goFromResources[c];
        lesModels[u].startAnim();
    }

    public void readyOrUnreadyUser(int u_num, bool isHeReady)
    {
        panelPlayer.readyOrUnreadyUser(u_num, isHeReady);
    }

    public void switchTeamColor(User u, MenuManager.Team t)
    {
        switch(t)
        {
            case MenuManager.Team.blue:
                lesModels[u].imageRond.color = new UnityEngine.Color(68f, 105f, 238f);
                break;

            case MenuManager.Team.green:
                lesModels[u].imageRond.color = new UnityEngine.Color(16f, 201f, 63f);
                break;

            case MenuManager.Team.red:
                lesModels[u].imageRond.color = new UnityEngine.Color(253f, 16f, 16f);
                break;

            case MenuManager.Team.yellow:
                lesModels[u].imageRond.color = new UnityEngine.Color(248f, 251f, 39f);
                break;

            default: // white
                lesModels[u].imageRond.color = new UnityEngine.Color(0, 0, 0);
                break;
        }
    }
}
