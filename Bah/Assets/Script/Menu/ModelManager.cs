﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ModelManager : MonoBehaviour {
    
    [SerializeField]
    private GameObject[] lesPlayers;

    public void init()
    {
        GameObject Left_modelCanvas = gameObject.transform.FindChild("Left").gameObject;
        GameObject Right_modelCanvas = gameObject.transform.FindChild("Right").gameObject;

        int cellSize_X = (int)(Left_modelCanvas.GetComponent<RectTransform>().rect.width - 10) / 2;
        int cellSize_Y = (int)(Left_modelCanvas.GetComponent<RectTransform>().rect.height - 10);

        Left_modelCanvas.GetComponent<GridLayoutGroup>().cellSize = new Vector2(cellSize_X, cellSize_Y);
        Right_modelCanvas.GetComponent<GridLayoutGroup>().cellSize = new Vector2(cellSize_X, cellSize_Y);
    }

    public void activeOrDesactiveUser(User u, bool isHeActive)
    {
        lesPlayers[u._numPlayer - 1].SetActive(isHeActive);
    }

    public Model_MenuManager getModel(User u)
    {
        return lesPlayers[u._numPlayer - 1].GetComponent<Model_MenuManager>();
    }
}
