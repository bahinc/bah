﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerPanel : MonoBehaviour {

    [SerializeField]
    private GameObject[] lesPlayers;

    [SerializeField]
    private Sprite Sprite_keyboard;

    [SerializeField]
    private Sprite Sprite_controller;

    public void readyOrUnreadyUser(int u_num, bool isHeReady)
    {
        if (u_num < 1 || u_num > 4) Debug.LogError("Index de User invalide : " + u_num);
        lesPlayers[u_num - 1].transform.Find("ready/icon").gameObject.SetActive(isHeReady);
    }

    public void activeOrDesactiveUser(User u, bool isHeActive)
    {
        lesPlayers[u._numPlayer - 1].SetActive(isHeActive);
        if (isHeActive) lesPlayers[u._numPlayer - 1].GetComponentInChildren<Image>().sprite = u.isKeyboardUsed ? Sprite_keyboard : Sprite_controller;
    }
}
