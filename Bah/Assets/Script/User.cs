﻿using UnityEngine;
using System.Collections;

public class User : MonoBehaviour {

    public int _numPlayer;

    [SerializeField]
    private string _numController;

    private Character _choosenCharacter;

    public Character choosenCharacter
    {
        private set { }
        get { return _choosenCharacter; }
    }

    private float _deadZone = 0.99f;

    private bool ready = false;
    
    private MenuManager _manager;

    private bool isInit = false;

    public bool didHeMove = false;

    private RectTransform _ownRect;

    private bool keyboardIsUsed;

    public bool isKeyboardUsed
    {
        private set { }
        get
        {
            return keyboardIsUsed;
        }
    }

    public MenuManager.Team team;

    private bool CoroutineCantSelectIsRunning = false;

    public void Init(MenuManager manager, int index, Character character, string howDoYouPlay)
    {
        _numPlayer = index;
        _manager = manager;
        _choosenCharacter = character;
        _ownRect = gameObject.GetComponent<RectTransform>();
        _numController = howDoYouPlay;
        placeCadre(_choosenCharacter);
        team = MenuManager.Team.white;
        keyboardIsUsed = _numController.Contains("Keyboard");
        isInit = true;
    }

    void Update ()
    {
        if (isInit)
        {
            if (!ready)
            {
                float X = keyboardIsUsed ? RebindableInput.GetAxis("Horizontal" + _numController) : Input.GetAxis("Horizontal" + _numController);
                float Y = keyboardIsUsed ? RebindableInput.GetAxis("Vertical" + _numController) : Input.GetAxis("Vertical" + _numController);
                //Debug.Log(X + " | " + Y);
                X = Mathf.Abs(X) < _deadZone ? 0 : X;
                Y = Mathf.Abs(Y) < _deadZone ? 0 : Y;
                Vector2 direction = new Vector2(X, Y);
                //Debug.Log(direction);
                if (direction != Vector2.zero && !didHeMove)
                {
                    X = X != 0 ? Mathf.Sign(X) : 0;
                    Y = Y != 0 ? Mathf.Sign(Y) : 0;
                    //Debug.Log(X + " | " + Y);
                    int actualIndex = _choosenCharacter.index;
                    int indexLook = actualIndex + (int)X + ((int)Y * 5);
                    indexLook = MakeIndexCorrect(indexLook);
                    _choosenCharacter = _manager.lesCharacters[indexLook];
                    placeCadre(_choosenCharacter);
                    didHeMove = true;
                }
                else if (direction == Vector2.zero && didHeMove)
                {
                    didHeMove = false;
                }
            }

            if (RebindableInput.GetKeyDown("Jump" + _numController))

                if (!ready) _manager.SelectCharacter(_numPlayer, _choosenCharacter);
                else _manager.RequestOfStarting();
                

            if (RebindableInput.GetKeyDown("Cancel" + _numController))
                if (ready) _manager.UnselectCharacter(_numPlayer);
                else _manager.DestroyUser(this);

            if (RebindableInput.GetKeyDown("Stomp" + _numController))
                if (!ready)
                {
                    team++;
                    _manager.changeTeam(this, team);
                }
        }
    }

    private void placeCadre(Character _choosenOne)
    {
        transform.SetParent(_choosenCharacter.transform);
        transform.SetSiblingIndex(transform.parent.childCount-2);
        _ownRect.anchorMax = Vector2.one;
        _ownRect.anchorMin = Vector2.zero;
        _ownRect.localPosition = Vector2.zero;
        _ownRect.localScale = Vector3.one;
        _ownRect.offsetMax = Vector2.zero;
        _ownRect.offsetMin = Vector2.zero;
        if (isInit) _manager.placeModel(this, _choosenOne);
    }

    private int MakeIndexCorrect(int look)
    {
        if (look <= 0) look = 1;
        else if (look > _manager.lesCharacters.Count) look = _manager.lesCharacters.Count;
        return look;
    }

    public void animSelect()
    {
        float bottom;
        float top;
        float left;
        float right;
        switch(_numPlayer)
        {
            case 1:
                bottom = -100f;
                top = 0f;
                right = 100f;
                left = 0;
                break;

            case 2:
                bottom = -100f;
                top = 0f;
                right = 0f;
                left = -100f;
                break;

            case 3:
                bottom = 0f;
                top = -100f;
                right = 0f;
                left = 100f;
                break;

            case 4:
                bottom = 0f;
                top = -100f;
                right = 100f;
                left = 0;
                break;

            default:
                bottom = 0;
                top = 0;
                left = 0;
                right = 0;
                break;
        }
        StartCoroutine(Select(bottom, top, left, right));
        ready = true;
    }

    public void animCantSelect()
    {
        if (CoroutineCantSelectIsRunning) return;
        float bottom;
        float top;
        float left;
        float right;
        switch (_numPlayer)
        {
            case 1:
                bottom = -50f;
                top = 0f;
                right = 50f;
                left = 0;
                break;

            case 2:
                bottom = -50f;
                top = 0f;
                right = 0f;
                left = -50f;
                break;

            case 3:
                bottom = 0f;
                top = -50f;
                right = 0f;
                left = 50f;
                break;

            case 4:
                bottom = 0f;
                top = -50f;
                right = 50f;
                left = 0;
                break;

            default:
                bottom = 0;
                top = 0;
                left = 0;
                right = 0;
                break;
        }
        StartCoroutine(cantSelect(bottom, top, left, right));
    }

    public void animDeselect()
    {
        float bottom;
        float top;
        float left;
        float right;
        switch (_numPlayer)
        {
            case 1:
                bottom = -100f;
                top = 0f;
                right = 100f;
                left = 0;
                break;

            case 2:
                bottom = -100f;
                top = 0f;
                right = 0f;
                left = -100f;
                break;

            case 3:
                bottom = 0f;
                top = -100f;
                right = 0f;
                left = 100f;
                break;

            case 4:
                bottom = 0f;
                top = -100f;
                right = 100f;
                left = 0;
                break;

            default:
                bottom = 0;
                top = 0;
                left = 0;
                right = 0;
                break;
        }
        StartCoroutine(DeSelect(bottom, top, left, right));
        ready = false;
    }

    private IEnumerator Select(float bottom, float top, float left, float right)
    {
        float timeElapsed = 0;
        float duration = 0.5f;
        float bottomActual = 0;
        float rightActual = 0;
        float topActual = 0;
        float leftActual = 0;
        while (timeElapsed < duration + 0.05f)
        {
            bottomActual = Mathf.Lerp(0, bottom, timeElapsed / duration);
            rightActual = Mathf.Lerp(0, right, timeElapsed / duration);
            topActual = Mathf.Lerp(0, top, timeElapsed / duration);
            leftActual = Mathf.Lerp(0, left, timeElapsed / duration);
            _ownRect.offsetMax = new Vector2(rightActual, topActual);
            _ownRect.offsetMin = new Vector2(leftActual, bottomActual);
            timeElapsed += Time.deltaTime;
            yield return null;
        }
    }

    private IEnumerator DeSelect(float bottom, float top, float left, float right)
    {
        float timeElapsed = 0;
        float duration = 0.5f;
        float bottomActual = -150f;
        float rightActual = 100f;
        float topActual = 100f;
        float leftActual = -150f;
        while (timeElapsed < duration + 0.05f)
        {
            bottomActual = Mathf.Lerp(bottom, 0, timeElapsed / duration);
            rightActual = Mathf.Lerp(right,0, timeElapsed / duration);
            topActual = Mathf.Lerp(top, 0, timeElapsed / duration);
            leftActual = Mathf.Lerp(left, 0, timeElapsed / duration);
            _ownRect.offsetMax = new Vector2(rightActual, topActual);
            _ownRect.offsetMin = new Vector2(leftActual, bottomActual);
            timeElapsed += Time.deltaTime;
            yield return null;
        }
    }

    private IEnumerator cantSelect(float bottom, float top, float left, float right)
    {
        CoroutineCantSelectIsRunning = true;
        float timeElapsed = 0;
        float duration = 0.3f;
        float bottomActual = 0f;
        float rightActual = 0f;
        float topActual = 0f;
        float leftActual = 0f;
        while (timeElapsed < duration + 0.05f)
        {
            bottomActual = Mathf.Lerp(0, bottom, timeElapsed / duration);
            rightActual = Mathf.Lerp(0, right, timeElapsed / duration);
            topActual = Mathf.Lerp(0, top, timeElapsed / duration);
            leftActual = Mathf.Lerp(0, left, timeElapsed / duration);
            _ownRect.offsetMax = new Vector2(rightActual, topActual);
            _ownRect.offsetMin = new Vector2(leftActual, bottomActual);
            timeElapsed += Time.deltaTime;
            yield return null;
        }
        timeElapsed = 0;
        while (timeElapsed < duration + 0.05f)
        {
            bottomActual = Mathf.Lerp(bottom, 0, timeElapsed / duration);
            rightActual = Mathf.Lerp(right, 0, timeElapsed / duration);
            topActual = Mathf.Lerp(top, 0, timeElapsed / duration);
            leftActual = Mathf.Lerp(left, 0, timeElapsed / duration);
            _ownRect.offsetMax = new Vector2(rightActual, topActual);
            _ownRect.offsetMin = new Vector2(leftActual, bottomActual);
            timeElapsed += Time.deltaTime;
            yield return null;
        }
        CoroutineCantSelectIsRunning = false;
    }
}
