﻿using UnityEngine;
using System.Collections;

public class partCollider : MonoBehaviour {

    private GameObject character;

    void Start()
    {
        character = transform.parent.gameObject;
    }

	void OnTrigger2DEnter(Collision2D col)
    {
        Debug.Log(name + " | " + col.collider + " trigger");
    }

    void OnCollision2DEnter(Collision2D col)
    {
        Debug.Log(name + " | " + col.collider + " col");
    }
}
