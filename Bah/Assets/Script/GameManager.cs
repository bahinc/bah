﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class GameManager : MonoBehaviour {

    private const float boundariesOFMap = 8.40f;
    private bool testSp = false;

    public List<player2D> _playersList;

    [SerializeField]
    private Dictionary<player2D, bool> _actualPlayerOfTheRound;

    public List<Vector3> _lesStartsPositions;

    [SerializeField]
    private Dictionary<player2D, Vector3> _playerStartPosition;

    private GameObject _canvasGame;

    private GuiGame _canvasUi;

    private GameObject _versusText;

    private float cameraOrthographicSize = 5f;

    private Coroutine createSuperBoxCor;

    private bool roundIsOver = false;

    public Dictionary<player2D, int> listVie;

    private List<SuperBox> _lesSuperBox;

    public void Init(Dictionary<int, Character> lesPlayers, Dictionary<int, string> _howDoTheyPlay)
    {
        _playersList = new List<player2D>();
        _actualPlayerOfTheRound = new Dictionary<player2D, bool>();
        for(int i = 1; i <= lesPlayers.Count; i++)
        {
            GameObject playerGo = Instantiate(Resources.Load("players/" + lesPlayers[i].nom) as GameObject);
            playerGo.transform.localScale = Vector3.one;
            player2D aPlayer = playerGo.GetComponent<player2D>();
            aPlayer.enabled = true;
            aPlayer.InitPlayer(this, i, _howDoTheyPlay[i]);
            playerGo.SetActive(false);
            _playersList.Add(aPlayer);
            _actualPlayerOfTheRound.Add(aPlayer, true);
        }
        _playerStartPosition = new Dictionary<player2D, Vector3>();
        _lesSuperBox = new List<SuperBox>();
        _lesStartsPositions = new List<Vector3>();
        _lesStartsPositions.Add(new Vector3(-6.92f, 0.5f, 0));
        _lesStartsPositions.Add(new Vector3(5f, 0.5f, 0));
        for (int i = 0; i < _lesStartsPositions.Count; i++) _playerStartPosition.Add(_playersList[i], _lesStartsPositions[i]);
        StartCoroutine(SpawnCharacter(_playersList));

        listVie = new Dictionary<player2D, int>();
        foreach (player2D _p in _playersList) listVie.Add(_p, 5);

        _canvasGame = Instantiate(Resources.Load("CanvasGame") as GameObject);
        _canvasUi = _canvasGame.AddComponent<GuiGame>();
        _canvasUi.Init(_playersList, this);

        _versusText = _canvasGame.transform.FindChild("VS text").gameObject;
    }

    void Start()
    {

        //_playersList = new List<player2D>();
    }

    public void RemoveMeFromTheRound(player2D _dead)
    {
        foreach (player2D _p in _playersList)
        {
            //Debug.Log(_p.name);
            if (_p == _dead) _actualPlayerOfTheRound[_p] = false;
        }
        if (numberOfPlayerAlive() == 1)
        {
            roundIsOver = true;
            foreach (player2D _p in _playersList)
                if (_actualPlayerOfTheRound[_p])
                    StartCoroutine(ShowRoundWinner(_p));
        }
    }

    private int numberOfPlayerAlive()
    {
        int cpt = 0;
        foreach (player2D _p in _playersList)
            if (_actualPlayerOfTheRound[_p])
                cpt++;
        return cpt;
    }

    public void KillPlayerByPlayer(player2D _player, player2D _killer)
    {
        _canvasGame.GetComponent<GuiGame>().removeHeart(_player);
        listVie[_player] -= 1;
        removeLayerOfTheDeadPlayer(_player);
        KillCharacterByPlayer(_player, _killer);
    }

    private void removeLayerOfTheDeadPlayer(player2D _player)
    {
        foreach (player2D _p in _playersList)
        {
            if(_p != _player)
            {
                _p.removePlayerFromCollision("player" + _player.playerNumber);
            }
        }
    }

    public void StunPlayer(player2D _player, player2D _stunnedPlayer)
    {
        float direction = Mathf.Sign(_stunnedPlayer.transform.position.x - _player.transform.position.x);
        _stunnedPlayer.GetStun(direction);
    }

    private float DirectionBetweenTwoPlayers(player2D _player, player2D _killer)
    {
        float direction = Mathf.Sign(_player.transform.position.x - _killer.transform.position.x);
        return direction;
    }

    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.U)) _playersList[0].gameObject.SetActive(true);
        //if (Input.GetKeyDown(KeyCode.Y)) _playersList[1].gameObject.SetActive(true);
    }

    private void ResetGame()
    {
        Vector3 spawnPoint;
        testSp = false;
        foreach (player2D _p in _playersList)
        {
            spawnPoint = _playerStartPosition[_p];
            _actualPlayerOfTheRound[_p] = true;
            _p.transform.position = _playerStartPosition[_p];
            _p.GetComponent<SpriteRenderer>().flipX = spawnPoint.x >= 0;
            _p.Reset();
        }
        foreach (SuperBox sp in _lesSuperBox) removeSuperBox(sp); // On détruit les superBox
        ResetCamera();
        StartCoroutine(VersusAnim());
    }

    private void ResetCamera()
    {
        Camera _cam = Camera.main;
        _cam.transform.position = Vector3.forward * -10;
        _cam.orthographicSize = cameraOrthographicSize;
    }

    private void KillCharacterByPlayer(player2D _playerToDie, player2D _killerplayer)
    {
        //TODO - Set the Death Animation for the player
        //foreach(player2D _p in _playersList) _p.CanYouMove(false);
        float _direction = DirectionBetweenTwoPlayers(_playerToDie, _killerplayer);
        _playerToDie.DeathAnim(_direction);
    }

    public void removeSuperBox(SuperBox sp)
    {
        if (_lesSuperBox.Count <= 0) Debug.LogError("la liste des SuperBox est vide, pourtant, on tente d'en supprimer une.");
        Debug.Log(_lesSuperBox.Count);
        foreach (SuperBox s in _lesSuperBox)
        {
            Debug.Log(s.transform.position);
            if (testSp) Debug.LogError("On est retourné dans le foreach alors que l'on aurait pas dû");
            if (s == sp)
            {
                _lesSuperBox.Remove(s);
                testSp = true;
                break;
            }
        }
        Destroy(sp.gameObject);
    }

    public void stopSuperBox(List<SuperBox> lsp)
    {
        foreach(SuperBox sp in lsp) sp.stopTheBox();
    }

    private IEnumerator SpawnCharacter(List<player2D> _listcharacter)
    {
        float duration = 2f;
        Vector3 spawnPoint;

        foreach (player2D _character in _listcharacter)
        {
            spawnPoint = _playerStartPosition[_character];

            //TODO - set a spawn Animation
            _character.gameObject.transform.position = spawnPoint;
            _character.GetComponent<SpriteRenderer>().flipX = spawnPoint.x >= 0;
            _character.gameObject.SetActive(true);

            Camera _cam = Camera.main;
            Vector3 cameraOriginMovement = _cam.transform.position;
            Vector3 cameraTarget = new Vector3(spawnPoint.x, spawnPoint.y, -1);
            float cameraActualSize = _cam.orthographicSize;
            float timeElapsed = 0;
            float targetSize = 2f;
            while (timeElapsed < duration + 0.05f)
            {
                _cam.transform.position = Vector3.Lerp(cameraOriginMovement, cameraTarget, timeElapsed / duration);
                _cam.orthographicSize = Mathf.Lerp(cameraActualSize, targetSize, timeElapsed / duration);
                timeElapsed += Time.deltaTime;
                yield return null;
            }
        }
        StartCoroutine(GetCameraBackInPosition(duration));
    }

    private IEnumerator GetCameraBackInPosition(float duration)
    {
        Camera _cam = Camera.main;
        Vector3 cameraOriginMovement = _cam.transform.position;
        Vector3 cameraTarget = new Vector3(0, 0, -1);
        float cameraActualSize = _cam.orthographicSize;
        float timeElapsed = 0;
        while (timeElapsed < duration + 0.05f)
        {
            _cam.transform.position = Vector3.Lerp(cameraOriginMovement, cameraTarget, timeElapsed / duration);
            _cam.orthographicSize = Mathf.Lerp(cameraActualSize, cameraOrthographicSize, timeElapsed / duration);
            timeElapsed += Time.deltaTime;
            yield return null;
        }
        StartCoroutine(VersusAnim());
    }

    private IEnumerator FocusOnPlayer(player2D _player,float duration)
    {
        Vector3 spawnPoint = _player.gameObject.transform.position;

        Camera _cam = Camera.main;
        Vector3 cameraOriginMovement = _cam.transform.position;
        Vector3 cameraTarget = new Vector3(spawnPoint.x, spawnPoint.y, -1);
        float cameraActualSize = _cam.orthographicSize;
        float timeElapsed = 0;
        float targetSize = 2f;
        while (timeElapsed < duration + 0.05f)
        {
            _cam.transform.position = Vector3.Lerp(cameraOriginMovement, cameraTarget, timeElapsed / duration);
            _cam.orthographicSize = Mathf.Lerp(cameraActualSize, targetSize, timeElapsed / duration);
            timeElapsed += Time.deltaTime;
            yield return null;
        }
        yield return new WaitForSeconds(1f);
    }

    private IEnumerator VersusAnim()
    {
        setMoveToAllPlayers(false);
        _versusText.SetActive(true);

        RectTransform _vGO = _versusText.transform.FindChild("V").GetComponent<RectTransform>();
        RectTransform _sGO = _versusText.transform.FindChild("S").GetComponent<RectTransform>();

        Vector2 _middle = new Vector2(150, 0);
        Vector2 _extVert = new Vector2(150, 700);
        Vector2 _extHori = new Vector2(1100, 0);
        float timeElapsed = 0;
        float duration = 1.5f;
        while (timeElapsed < duration / 2 + 0.05f)
        {
            _vGO.localPosition = Vector2.Lerp(-_extVert, -_middle, timeElapsed / (duration / 2));
            _sGO.localPosition = Vector2.Lerp(_extVert, _middle, timeElapsed / (duration / 2));
            timeElapsed += Time.deltaTime;
            yield return null;
        }
        timeElapsed = 0;
        yield return new WaitForSeconds(0.5f);
        while (timeElapsed < duration / 2 + 0.05f)
        {
            _vGO.localPosition = Vector2.Lerp(-_middle, -_extHori, timeElapsed / (duration / 2));
            _sGO.localPosition = Vector2.Lerp(_middle, _extHori, timeElapsed / (duration / 2));
            timeElapsed += Time.deltaTime;
            yield return null;
        }
        _versusText.SetActive(false);

        float Duration = Random.Range(2f, 8f);
        float Probability = Random.Range(0f, 100f);
        createSuperBoxCor = StartCoroutine(createSuperBox(Duration, Probability));

        setMoveToAllPlayers(true);
    }

    private void setMoveToAllPlayers(bool move)
    {
        for (int i = 0; i < _playersList.Count; i++) {
            _playersList[i].CanYouMove(move);
            if (move) _playersList[i].ResetIdleBool();
        }
    }

    private IEnumerator ShowRoundWinner(player2D _winner)
    {
        StopCoroutine(createSuperBoxCor);
        stopSuperBox(_lesSuperBox);
        _winner.CanYouMove(false);
        StartCoroutine(FocusOnPlayer(_winner, 2f));
        yield return new WaitForSeconds(2f);
        //Set "Smile Anim" for the winner
        yield return new WaitForSeconds(1f);
        ResetGame();
    }

    private IEnumerator createSuperBox(float duration, float probability)
    {
        yield return new WaitForSeconds(duration);
        if (Random.Range(0f,100f) >= probability)
        {
            //Create, from Resources, the Super Box, then place it on the X-axis and init()
            GameObject crate = Instantiate(Resources.Load("PowerUpCrate") as GameObject);
            SuperBox sp = crate.GetComponent<SuperBox>();
            _lesSuperBox.Add(sp);
            sp.init(boundariesOFMap, this);
        }
        float newDuration = Random.Range(2f, 8f);
        float newProbability = Random.Range(0f, 100f);
        createSuperBoxCor = StartCoroutine(createSuperBox(newDuration, newProbability));
    }
}
